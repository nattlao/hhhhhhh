﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Rectangle
    {
        public static string ShapeName
        {
            get { return "Rectangle"; }
        }
        public double Length { get; set; }
        public double Width { get; set; }
        public double GetArea()
        {
            return this.Length * this.Width;
        }
    }
}
